package com.helloworldci

object Launcher {

  def main(args: Array[String]) = {
    val hello = new HelloWorld
    hello.printHelloWorld()
    println()
  }
}
