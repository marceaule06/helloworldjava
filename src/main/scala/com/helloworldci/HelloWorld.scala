package com.helloworldci

// This trait to intercepts print statements
trait Output {
  def print(s: String) = Console.print(s)
}

class HelloWorld extends Output {

  val helloWorldString = "Hello world"

  def printHelloWorld() = {
    print(helloWorldString)
  }

}
