package com.helloworldci

import org.scalatest.funsuite.AnyFunSuite

class HelloWorldTest extends AnyFunSuite{

  val expected_output = "Hello world"
  val hello = new HelloWorld

  test("Unit test n°1: Check string value member") {
    assert(expected_output == hello.helloWorldString)
  }

}
