package com.helloworldci

import org.scalatest.funsuite.AnyFunSuite

class HelloWorldTest extends AnyFunSuite{

  val expected_output = "Hello world"

  // This trait is to intercept the calls to print method
  trait MockOutput extends Output {
    var output_value: Seq[String] = Seq()
    override def print(s: String) = output_value = output_value :+ s
  }

  val hello = new HelloWorld with MockOutput

  test("Integration test n°1: Check print output value") {
    hello.printHelloWorld()
    assert(hello.output_value.contains(expected_output))
  }
}
