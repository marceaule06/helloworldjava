object Versions {
  // Project dependencies

  val scala = "2.13.7"

  val scalactic = "3.2.11"

  val scalatest = "3.2.11"

}
