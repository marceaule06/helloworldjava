# HelloWorldScala

Simple scala program to print "Hello world" on the standard output

## Getting started

Project organization

- **The scala code**: ```The source code is placed in /src/main/scala```
- **The functional test**: ```The functional tests are placed in /src/main/test/scala```
- **The integration test**: ```The integration tests are placed in /src/it/main/test/scala```
- **Project dependencies versions**: ```Find project dependencies versions in /project/Versions.scala```

## Run the project
To run the project
```
sbt run
```

## Running the tests
To execute the unit test
```
sbt test
```

## Running the integration tests
To execute the integration test
```
sbt "it:testOnly *"
```

## Build and package
To build and package the project
```
sbt assembly
```