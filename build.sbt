import sbt.project

scalaVersion := Versions.scala

// CI_PIPELINE_ID is gitlab env variable
val packageVersion = "1.0.0" + "_" + sys.env.getOrElse("CI_PIPELINE_ID", "release")

val Integration = config("it") extend Test
val helloWorldProject = (project in file(".")).configs(Integration).settings (

  name := "HelloWorldScala",
  version := packageVersion,
  scalaVersion := Versions.scala,

  libraryDependencies += "org.scalactic" %% "scalactic" % Versions.scalactic % "test",
  libraryDependencies += "org.scalatest" %% "scalatest" % Versions.scalatest % "it, test",

  // To publish releases to artifactory
  publishTo := Some("Artifactory repository" at "http://localhost:8081/artifactory/libs-release-local"),
  credentials += Credentials("Artifactory Realm", "localhost", sys.env("ARTIFACTORY_USER"), sys.env("ARTIFACTORY_PASSWORD")),

  // To publish snapshots to artifactory
  publishTo := Some("Artifactory Realm" at "https://helloworldscala.jfrog.io/artifactory/default-maven-local;build.timestamp=" + new java.util.Date().getTime),
  credentials += Credentials("Artifactory Realm", "helloworldscala.jfrog.io", sys.env("ARTIFACTORY_USER"), sys.env("ARTIFACTORY_PASSWORD"))

)